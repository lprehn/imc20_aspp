This repository contains artefacts for the paper:

**AS-Path Prepending: there is no rose without a thorn**<br>
Pedro Marcos (FURG); Lars Prehn (MPII); Lucas Leal (UFRGS); Alberto Dainotti (CAIDA, UC San Diego); Anja Feldmann (MPI); Marinho Barcellos (University of Waikato)

The paper can be found [here](./paper.pdf). 
The artefacts in this repository can be used to reproduce the main results of the paper or to extend our work. 
Since the data we used in our paper is rather large we decided to include *significantly* smaller sample data in:

~~~
/data_samples/
~~~

In most cases, the sample data alone _will not_ produce similar results to those presented in the paper; however, it will allow you to
understand how to work with the scripts and how to adjust them if necessary. 

The repository itself is structured as follows:

~~~
.
├── data_samples
│   ├── bgp                                                     # Samples of Route Collector data
│   │   ├── README.md
│   │   ├── ribs
│   │   │   └── 20200801
│   │   │       ├── ris_rrc12_20200801.0000.rib.gz
│   │   │       └── rov_nwax_20200801.0000.rib.gz
│   │   └── updates
│   │       ├── 20200801
│   │       │   ├── ris_rrc12
|   |       |   |   ├── ...
│   │       │   │   └── updates.20200801.2300.gz
│   │       │   └── rov_nwax
│   │       │       ├── ...
│   │       │       └── updates.20200801.2300.gz
│   │       └── 20200802
│   │           ├── ris_rrc12
│   │           │   ├── ...
│   │           │   └── updates.20200802.2300.gz
│   │           └── rov_nwax
│   │               ├── ...
│   │               └── updates.20200802.2300.gz
│   └── rir                                                     # Samples of RIR delegation files
│       └── 20200801
│           ├── ...
│           ├── delegated-ripencc-extended-20200801.bz2
│           └── README.md
├── meta_data                                                   # contains directory structure used by \
│   ├── prepending_policies                                     # scripts to save meta data. \
│   │   └── origin_based                                        # since meta data files tend to be rather\ 
│   │       ├── v4                                              # large, this entire directory is on \
│   │       │   ├── origin_aggregate                            # gitignore. The directory structure was \ 
│   │       │   └── prefix_origin                               # force-added. 
│   │       └── v6
│   │           ├── origin_aggregate
│   │           └── prefix_origin
│   ├── sane_routes
│   │   ├── v4
│   │   └── v6
│   └── visibility
├── scripts
│   ├── correlate_prepend_and_region                            # Reproduce results for Figure 9.
│   │   ├── calc_ppsize_rir_dist_prefixes.py
│   │   ├── README.md
│   │   └── rir_stream.py
│   ├── extract_policies                                        # Reproduce results for Figure 5, 6, and 7.
│   │   ├── aggregate_policies_to_origin_level.py
│   │   ├── extract_origin_prepending_per_prefix.py
│   │   └── README.md
│   ├── extract_sanitized_routes                                # Runs most of the sanitation, i.e., must be executed before most other scripts 
│   │   ├── apply_base_sanitation.py
│   │   ├── get_sane_routes_v4.sh
│   │   ├── get_sane_routes_v6.sh
│   │   ├── README.md
│   │   └── untangle_prefixes.py
│   ├── extract_usage                                           # Reproduce results for Figure 2 and 3. 
│   │   ├── apply_monitor_limit.py
│   │   ├── calculate_addresses.py
│   │   ├── calculate_prepend_ases.py
│   │   ├── get_usage_statistics.sh
│   │   ├── README.md
│   │   ├── read_only_sane_prepended_intermediate.py
│   │   ├── read_only_sane_prepended_origin.py
│   │   └── read_only_sane_prepended.py
│   ├── extract_visibility_of_primary_policy                    # Reproduce results for Figure 4.
│   │   ├── analyze_prepending_consistency.py
│   │   ├── calc_availability_and_consistency.py
│   │   ├── README.md
│   │   └── utils.py
│   ├── generate_peering_exp_schedules                          # Generate crontabs, traffic, and captures for the schedules in section 6.  
│   │   ├── configure_routing_for_experiment.py
│   │   ├── get_exp_crontab_and_timings.py
│   │   ├── get_hijack_exp_crontab.py
│   │   ├── README.md
│   │   ├── run_pings.py
│   │   ├── start_capture.py
│   │   └── timings.csv
│   └── README.md
└── tmp
~~~

Apart from instruction on how to execute the scripts on the sample data, the individual README.md files 
of each directory provide hints on how to change the code, on what each script depends, how long it took to complete
on our machine, as well as what output to expect. 

The maschine that carried out the data analysis presented in the paper had the following characteristics:

~~~
Cores: 128
RAM: 2.2 TB
HDD: 20 TB
OS: Debian GNU/Linux 9 (stretch)
~~~

We relied on the following versions for our python3.5.3 imports (all of which can be installed via pip3): 

~~~
ipaddress==1.0.22
pytricia==1.0.0
~~~

We further relied on the following command line tools for the active experiments:
~~~
Nping version 0.7.40
tcpdump version 4.9.2
GNU parallel 20161222
~~~

If you need any help, please contact:
lprehn@mpi-inf.mpg.de 