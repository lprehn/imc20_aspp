The data in those directories was downloaded from

~~~
http://archive.routeviews.org/route-views.nwax/bgpdata/2020.08/
http://data.ris.ripe.net/rrc12/2020.08/
~~~

and slightly preprocessed, e.g., in order to aggregate
update files to hourly granularity you can run something similar to: 

~~~
for h in 0{0..9} {10..23}; do bgpscanner updates.20200802.${h}*.bz2 | gzip > updates.20200802.${h}00.gz; done
~~~


