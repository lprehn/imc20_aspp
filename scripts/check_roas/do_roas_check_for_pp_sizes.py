import sys, subprocess, gzip
from pytricia import PyTricia

from collections import defaultdict

def read_rpki():

    # read the date
    year = sys.argv[1][:4]
    month = sys.argv[1][4:6]
    day = sys.argv[1][6:]
    DIR="../../data_samples/rpki/"+year+"/"+month+"/"+day+"/"

    # read all the data
    rirs =["afrinic.tal", "apnic-afrinic.tal", "apnic-arin.tal", "apnic-iana.tal", "apnic-lacnic.tal", "apnic-ripe.tal", "apnic.tal", "arin.tal", "lacnic.tal", "ripencc.tal"]
    for rir in rirs:
        with open(DIR+rir+'_roas_'+year+month+day+'.csv', 'r') as INPUT:
            header_read = False
            for line in INPUT:
                if not header_read:           # skip the header line
                    header_read = True
                    continue

                elems = line.split(',')
                # strip the as in front
                asn = elems[1][2:]
                # always use the max length
                pref = elems[2]
                if not ':' in pref:           # change this to enable IPv6
                    yield pref, asn


def build_rpki_tree():
    # builds an pytricia tree from the RPKI data -> also handles delegations correctly
    pyt = PyTricia(128)
    for pref, orig in read_rpki():
        try:
            pyt[pref] = orig.strip()
        except:
            pass
    return pyt

pyt = build_rpki_tree()

minpol_total = defaultdict(int)    # counts the _total_ number of entries within the policy file that shows a given minimal prepending policy
min_pol_roas = defaultdict(int)    # structure same as above, but only counts entries that also have roas
non_prep_total, non_prep_roas = (0,0)  # two simple counters instead of another dict. They do the same but for non-prepended prefixes

# reads a policy file
DIR = '../../meta_data/prepending_policies/origin_based/v4/prefix_origin/'
with gzip.open(DIR+'prefix_origin_polices_v4_'+sys.argv[1]+'.gz', 'rt') as INPUT:
    for line in INPUT:
        if line.startswith('#'):         # ignore the header
            continue

        pref, orig, numMonitors, total_pols, pol = line.split('|')
        if int(numMonitors) < 200:       # fixed monitor limit, I guess we could make this a param later?
            continue

        orig = orig.strip()
        pol = int(pol)
        if pol > 0:                      # this entry showed prepending
            minsize  = min(min([int(x) for x in total_pols.split(';')]), 4)
            minpol_total[minsize] += 1

            covered_by_orig = pyt.get(pref)   # is this prefix covered by a ROA?
            if covered_by_orig is None:       # Btw. this check is rather weak ...
                continue                      # Strictly, prefix and origin should match not only prefix ...
            else:
                min_pol_roas[minsize] += 1
        else:
            non_prep_total += 1               # Does the same check as above, but for not preprended prefixes
            covered_by_orig = pyt.get(pref)
            if covered_by_orig is None:
                continue
            else:
                non_prep_roas += 1

print('|'.join([ sys.argv[1], str(non_prep_roas), str(min_pol_roas[0]), str(min_pol_roas[1]), str(min_pol_roas[2]), str(min_pol_roas[3]), str(min_pol_roas[4]),
                              str(non_prep_total), str(minpol_total[0]), str(minpol_total[1]), str(minpol_total[2]), str(minpol_total[3]), str(minpol_total[4])]))
