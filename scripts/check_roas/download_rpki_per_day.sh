#!/bin/bash


if [ $# -eq 1 ]; then
    y=${1:0:4}
    m=${1:4:2}
    d=${1:6:2}
elif [ $# -eq 3 ]; then
    y=$1
    m=$2
    d=$3
else
    echo "wrong number of params"
    exit 1
fi

DIR=../../data_samples/rpki/${y}/${m}/${d}/
mkdir -p ${DIR}

declare -a StringArray=( "afrinic.tal" "apnic-afrinic.tal" "apnic-arin.tal" "apnic-iana.tal" "apnic-lacnic.tal" "apnic-ripe.tal" "apnic.tal" "arin.tal" "lacnic.tal" "ripencc.tal" )
for RIR in "${StringArray[@]}"; do
    wget https://ftp.ripe.net/rpki/${RIR}/${y}/${m}/${d}/roas.csv -O ${DIR}${RIR}_roas_${y}${m}${d}.csv
done
