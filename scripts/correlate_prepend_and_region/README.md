# ASPP Usage per Region

The scripts in this directory correlate ASPP usage with 
its service region. This can be used to reproduce Figure 
9 from the paper. 

# Scripts

python3 calc_ppsize_rir_dist_prefixes.py \<date\> \<NumMonitors\>
> this script reads sample data from ../../data_samples/rir/ 
> as well as previous results in 
> ../../meta_data/prepending_policies/origin_based/v4/prefix_origin/
> which can be generated using the scripts in ../extract_policies/.
> It calculates the maximum and minimum prepending size that was 
> observed for each prefix and to which service region the prefix 
> belongs; afterwards, it calculates statistics on those prefixes. 

rir_stream.py 
> this script is not executable. it holds functions to
> read (extended) RIR delegation files from ../../data_samples/rir/.
> It will _print_ warning messages, e.g., if the number of ipv4 
> entries in a delegation file does not match the file summary's prediction. 

# Required time

python3 calc_ppsize_rir_dist_prefixes.py 20200801 30

~~~
real    0m6.754s
user    0m6.690s
sys     0m0.055s
~~~

# Output

python3 calc_ppsize_rir_dist_prefixes.py 20200801 30

~~~
20200801|afrinic:0.05:0.53,0.11,0.15,0.20;0.37,0.19,0.11,0.33|apnic:0.32:0.44,0.15,0.15,0.26;0.29,0.20,0.21,0.31|arin:0.24:0.46,0.17,0.18,0.20;0.38,0.16,0.20,0.26|lacnic:0.12:0.32,0.20,0.17,0.31;0.21,0.20,0.21,0.39|ripencc:0.27:0.44,0.21,0.15,0.21;0.32,0.22,0.15,0.31
~~~

the top level format is:

~~~
<date>|<rir_info_1>|<rir_info_2>|<rir_info_3>|...
~~~

each rir_info is structured as follows:

~~~
<rir>:<fraction_of_all_prefixes_in_this_rir>:<min_pref_info>;<max_pref_info>
~~~

where min_pref_info and max_pref_info hold:

* Fraction of this RIR's prefixes with min/max prepend size 1
* Fraction of this RIR's prefixes with min/max prepend size 2
* Fraction of this RIR's prefixes with min/max prepend size 3
* Fraction of this RIR's prefixes with min/max prepend size 4 or more

separated by a comma. 
