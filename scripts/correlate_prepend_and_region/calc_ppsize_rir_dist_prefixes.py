from pytricia import PyTricia
from rir_stream import read_and_check
import sys, math, ipaddress, gzip
from collections import defaultdict


def subnet_size_to_prefix(space, size):
    '''
    returns none if there is no chance to place a prefix without hostbits into the subnet
    '''

    # get the base address that the theoretical prefix would have
    base = space[0] - (space[0] % (2**size))

    # base is to low, go higher.
    if base  < space[0]:
        base += (2**size)

    # now that base is inside, get the stop
    stop = base+(2**size)-1

    # if prefix fits into space, return success
    if base >= space[0] and stop <= space[1]:
        prefix = str(ipaddress.IPv4Address(base))+'/'+str(32 - size)
        return prefix, base, stop

    # otherwise return fail
    return None, None, None

def space_to_prefs(space):
    '''
    this fuction calculates the minimum prefixes needed to cover an ip range
    based on a (start_ip, stop_ip)-pair
    '''

    # only single address
    if space[1]-space[0] == 0:
            yield str(ipaddress.IPv4Address(space[0]))+'/32'
    else:
        # get the maximum number of host bits 
        max_pref = int(math.log(space[1]-space[0]+1, 2))
        # while there is still space to be covered
        while True:
            # fit into it the largest possible prefix 
            pref, start, stop = subnet_size_to_prefix(space, max_pref)
            # prefix does not fit in, restart trying with a smaller prefsize
            if pref is None:
                max_pref -= 1
                continue
            else:
                # there is a prefix that fits!
                yield pref
                # is there some rest at the start?
                if start != space[0]:
                    yield from space_to_prefs((space[0], start-1))
                # is there some rest at the end?
                if stop != space[1]:
                    yield from space_to_prefs((stop+1, space[1]))
                return

def convert_ranges_to_prefs(compressed):
    '''
    converts a list of address ranges into a list of 
    prefixes (that properly cover the address range)
    '''
    prefs = []
    for space in compressed:
        for pref in space_to_prefs(space):
            prefs.append(pref)
    return prefs

def get_rir_prefs(dir, date, rir):
    '''
    first reads a list of given out address ranges and
    then converts them into a prefix representation
    '''
    # ripe is bz2, be careful...
    if rir == 'ripencc':
        wb = read_and_check(dir+'/'+date+'/delegated-'+rir+'-extended-'+date+'.bz2', v4 = True, v6 = False, asn = False)
    else:
        wb = read_and_check(dir+'/'+date+'/delegated-'+rir+'-extended-'+date+'.gz', v4 = True, v6 = False, asn = False)

    # get ranges
    spaces = []
    for entry in wb:
        # check that the range is given out
        if entry[3] not in ['available', 'reserved']:
            spaces.append((entry[0], entry[1]))

    # convert to prefixes
    prefs = convert_ranges_to_prefs(spaces)
    return prefs 

def buildstring(minpols, maxpols):

    finalstring = []
    total_prefs = 0
    for rir in sorted(['afrinic', 'apnic', 'arin', 'lacnic', 'ripencc']):
        total_prefs += minpols[rir][1] + minpols[rir][2]+ minpols[rir][3]+ minpols[rir][4]

    
    for rir in sorted(['afrinic', 'apnic', 'arin', 'lacnic', 'ripencc']):
        minstr = []
        maxstr = []
        minsum = minpols[rir][1] + minpols[rir][2]+ minpols[rir][3]+ minpols[rir][4]
        maxsum = maxpols[rir][1] + maxpols[rir][2]+ maxpols[rir][3]+ maxpols[rir][4]
        #print(rir, str(minsum), str(maxsum))
        for ppsize in [1,2,3,4]:
            try:
                minfrac = float(minpols[rir][ppsize])/float(minsum)
            except:
                minfrac = 0.0
            try:
                maxfrac = float(maxpols[rir][ppsize])/float(maxsum)
            except:
                maxfrac = 0.0
            minstr.append("%.2f" % minfrac)
            maxstr.append("%.2f" % maxfrac)
        try:
            preffrac =float(minsum)/float(total_prefs)
        except:
            preffrac = 0.0
        finalstring.append(rir+':'+"%.2f" % preffrac+':'+','.join(minstr)+';'+','.join(maxstr))
    return '|'.join(finalstring)

# get the address to location mapping
DIR = '../../data_samples/rir/'
pyt = PyTricia(128)

# read the address range to location mapping
for rir in ['afrinic', 'apnic', 'arin', 'lacnic', 'ripencc']:
    prefs = get_rir_prefs(DIR, sys.argv[1], rir)
    for pref in prefs:
        pyt.insert(pref, rir)

# time to read the policies ...
PREFDIR= '../../meta_data/prepending_policies/origin_based/v4/prefix_origin/'
minpols = defaultdict(lambda: defaultdict(int))
maxpols = defaultdict(lambda: defaultdict(int))

lim=int(sys.argv[2])

with gzip.open(PREFDIR+'prefix_origin_polices_v4_'+sys.argv[1]+'.gz', 'rt') as INPUT:
    for line in INPUT:
        if line.startswith('#'):
            continue
        pref, orig, num_mon, pps, pol = line.split('|')
        if int(num_mon) < lim:
            continue
        if int(pol) == 0:
            continue
        allpols = [int(x) for x in pps.split(';')]
        allpppols = [x for x in allpols if x != 0]
        rir = pyt.get(pref)
        if rir is None:
            rir = 'unknown'
        minpols[rir][min(4,min(allpppols))] += 1
        maxpols[rir][min(4,max(allpppols))] += 1

print(sys.argv[1]+'|'+buildstring(minpols, maxpols))
