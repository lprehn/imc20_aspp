import gzip
import bz2
import ipaddress
import sys

magic_dict = {
    "\x1f\x8b\x08": (gzip.open, 'rt'),
    "\x42\x5a\x68": (bz2.BZ2File, 'r')
}
max_len = max(len(x) for x in magic_dict)

def smartopen(filename):
    if filename.endswith('.gz'):
        return gzip.open(filename, 'rt')
    elif filename.endswith('.bz2'):
        return bz2.open(filename, 'rt')
    else:
        return open(filename, 'r')

def read_and_check(filename, v4 = True, v6 = True, asn = True):
    '''
    returns 4 tuples: (start, stop, time, status)
    where stop is the last *included* resource
    '''
    ctrl = v4 + v6 + asn
    if ctrl == 0:
        return None

    # parsing bools 
    wasread_head = False
    wasread_sumv4 = False
    wasread_sumv6 = False
    wasread_sumasn = False

    # summary validation
    sumv4 = -1
    sumv6 = -1
    sumasn = -1

    status_v4 = []
    status_v6 = []
    status_asn = []

    with smartopen(filename) as INPUT:
        for line in INPUT: 
            # line is comment or empty
            if line.startswith('#') or not line.strip():
                continue

            # reading the header
            if not wasread_head:
                # actually parse the line HERE if you need its infos.
                wasread_head = True

            # reading the summaries
            elif not (wasread_sumv4 and wasread_sumv6 and wasread_sumasn):
                elem = line.split('|')
                if elem[-1].strip() != "summary":
                    print("Not all summaries are read, yet, there is a resource line.")
                type = elem[2].strip()
                if type == 'asn':
                    sumasn = int(elem[4])
                    wasread_sumasn = True
                elif type == 'ipv4':
                    sumv4 = int(elem[4])
                    wasread_sumv4 = True
                elif type == 'ipv6':
                    sumv6 = int(elem[4])
                    wasread_sumv6 = True
                else:
                    print("Found summary of unknown type.")
                    print(line)
            else:
                # only records here 
                elem = line.split('|')
                type = elem[2].strip()
                if type == 'asn':
                    if not asn: continue
                    start = int(elem[3])
                    width = int(elem[4])
                    status_asn.append((start, start+width-1, elem[5].strip(), elem[6].strip()))
                elif type == 'ipv4':
                    if not v4: continue
                    start = int(ipaddress.IPv4Address(elem[3]))
                    stop = start + int(elem[4]) -1
                    status_v4.append((start, stop, elem[5].strip(), elem[6].strip()))
                elif type == 'ipv6':
                    if not v6: continue
                    start = int(ipaddress.IPv6Address(elem[3]))
                    stop = start + int(elem[4]) -1
                    status_v6.append((start, stop, elem[5].strip(), elem[6].strip()))
                else:
                    print('dunno type:'+type)
    wb = []
    if v4:
        if len(status_v4) != sumv4:
            print('There should be '+str(sumv4)+' many IPv4 entries; however, there are only '+str(len(status_v4))+'.')
        wb.append(status_v4)
    if v6:
        if len(status_v6) != sumv6:
            print('There should be '+str(sumv6)+' many IPv4 entries; however, there are only '+str(len(status_v6))+'.')
        wb.append(status_v6)
    if asn:
        if len(status_asn) != sumasn:
            print('There should be '+str(sumasn)+' many IPv4 entries; however, there are only '+str(len(status_asn))+'.')
        wb.append(status_asn)
    
    if ctrl == 1:
        return wb[0]
    else:
        return wb

def transform_pref_to_irr_format(prefix):
    base_raw, cidr = prefix.split('/')
    base = int(ipaddress.IPv4Address(base_raw))
    stop = base + (2**(32-int(cidr))) -1
    return (base, stop, ' ', 'reserved')

