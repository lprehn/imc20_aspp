import sys, gzip, os
from collections import defaultdict

def calc_policies_for_file(file_in, file_out):

    if os.path.isfile(file_out):
        print(file_out+' already exists. Skipping.')
        return

    # monitor limit
    LIM = int(sys.argv[2])

    policies = defaultdict(lambda: defaultdict(set))
    with gzip.open(file_in, 'rt') as INPUT:
        for line in INPUT:
            # Prefix | Origin | NumMonitors | list(;):ObservedPrepends | resultingPolicy\n
            if line.startswith('#'):
                continue
            try:
                pref, orig, numMon, _, pol = line.split('|')
            except:
                print(line)
            if int(numMon) < LIM:
                continue
            policies[orig][int(pol)].add(pref)

    with gzip.open(file_out, 'wt+') as out:
        out.write('# Origin | pol | list(;):elem(pol,numPrefs):polsByPrefixSplit\n')
        for orig in policies:
            as_ppol = max(policies[orig].keys())
            polsplit=[]
            for pol in policies[orig].keys():
                polsplit.append(str(pol)+','+str(len(policies[orig][pol])))
            out.write(str(orig)+'|'+str(as_ppol)+'|'+';'.join(polsplit)+'\n')

    print('Finished calculating '+file_out)


# check availability of input files
v4, v6 = False, False
if os.path.isfile('../../meta_data/prepending_policies/origin_based/v4/prefix_origin/prefix_origin_polices_v4_'+sys.argv[1]+'.gz'):
    v4 = True

if os.path.isfile('../../meta_data/prepending_policies/origin_based/v6/prefix_origin/prefix_origin_polices_v6_'+sys.argv[1]+'.gz'):
    v6 = True

if v4:
    file_in = '../../meta_data/prepending_policies/origin_based/v4/prefix_origin/prefix_origin_polices_v4_'+sys.argv[1]+'.gz'
    file_out = '../../meta_data/prepending_policies/origin_based/v4/origin_aggregate/origin_polices_v4_'+sys.argv[1]+'.gz'
    calc_policies_for_file(file_in, file_out)
else:
    print('../../meta_data/prepending_policies/origin_based/v4/prefix_origin/prefix_origin_polices_v4_'+sys.argv[1]+'.gz does not exist. Cannot run v4 calculations.')

if v6:
    file_in = '../../meta_data/prepending_policies/origin_based/v6/prefix_origin/prefix_origin_polices_v6_'+sys.argv[1]+'.gz'
    file_out = '../../meta_data/prepending_policies/origin_based/v6/origin_aggregate/origin_polices_v6_'+sys.argv[1]+'.gz'
    calc_policies_for_file(file_in, file_out)
else:
    print('../../meta_data/prepending_policies/origin_based/v4/prefix_origin/prefix_origin_polices_v4_'+sys.argv[1]+'.gz does not exist. Cannot run v6 calculations.')

