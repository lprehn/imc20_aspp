import sys, os.path, gzip, subprocess
from collections import defaultdict


def zcat(FILE):
    # reads entire file into memory using zcat
    # significantly outpaces gzip.open() for large files.
    # requires sufficient RAM...
    process = subprocess.Popen(['zcat', FILE], stdout=subprocess.PIPE)
    for line in process.stdout:
        yield line.decode('utf-8').rstrip()

def get_num_origin_prepends(path):
    # calculates the number of times the origin repeats
    prepend = 0
    for i in  list(range(len(path)))[:-1][::-1]:
        if path[i] == path[-1]:
            prepend += 1
        else:
            return prepend
    return prepend

def calc_policies_for_file(file_in, file_out):
    if os.path.isfile(file_out):
        print(file_out+' already exists. Skipping.')
        return

    # dict for policies per (prefix,origin)-pair and the number of feeder
    # ASes that sees them.
    monitors = defaultdict(lambda : defaultdict(set))
    prepend_policies = defaultdict(lambda: defaultdict(set))

    for line in zcat(file_in):

        prefix, raw_path = line.split('|')

        # skip AS_PATHs with sets 
        if '{' in raw_path: 
            continue

        # straight forward collection of prependsizes per origin
        path = [int(x) for x in raw_path.split() if x.isdigit()]
        prepend_policies[prefix][path[-1]].add(get_num_origin_prepends(path))
        monitors[prefix][path[-1]].add(path[0])

    # calculate policy based on seen prependsizes per origin
    with gzip.open(file_out, 'wt+') as out:
        out.write('# Prefix | Origin | NumMonitors | list(;):ObservedPrepends | resultingPolicy\n')
        for prefix in prepend_policies:
            for origin in prepend_policies.get(prefix).keys():
                pols = prepend_policies[prefix][origin]
                N = len(pols)
                if N == 1:
                    if list(pols)[0] == 0:
                        ppol = 0
                    else:
                        ppol = 1
                elif N == 2:
                    ppol = 2
                else:
                    ppol = 3
                out.write(str(prefix)+'|'+str(origin)+'|'+str(len(monitors[prefix][origin]))+'|'+';'.join([str(x) for x in prepend_policies[prefix][origin]])+'|'+str(ppol)+'\n')
    print('Finished calculating '+file_out)

# check availability of input files
v4, v6 = False, False
if os.path.isfile('../../meta_data/sane_routes/v4/sane_routes_v4_'+sys.argv[1]+'.gz'):
    v4 = True

if os.path.isfile('../../meta_data/sane_routes/v6/sane_routes_v6_'+sys.argv[1]+'.gz'):
    v6 = True

if not v4 and not v6:
    print('Neither of the following input files exists:')
    print('../../meta_data/sane_routes/v4/sane_routes_v4_'+sys.argv[1]+'.gz')
    print('../../meta_data/sane_routes/v6/sane_routes_v6_'+sys.argv[1]+'.gz')
    sys.exit(2)

if v4:
    file_in = '../../meta_data/sane_routes/v4/sane_routes_v4_'+sys.argv[1]+'.gz'
    file_out = '../../meta_data/prepending_policies/origin_based/v4/prefix_origin/prefix_origin_polices_v4_'+sys.argv[1]+'.gz'
    calc_policies_for_file(file_in, file_out)
else:
    print('../../meta_data/sane_routes/v4/sane_routes_v4_'+sys.argv[1]+'.gz does not exist. Cannot run v4 calculations.')

if v6:
    file_in = '../../meta_data/sane_routes/v6/sane_routes_v6_'+sys.argv[1]+'.gz'
    file_out = '../../meta_data/prepending_policies/origin_based/v6/prefix_origin/prefix_origin_polices_v6_'+sys.argv[1]+'.gz'
    calc_policies_for_file(file_in, file_out)
else:
    print('../../meta_data/sane_routes/v6/sane_routes_v6_'+sys.argv[1]+'.gz does not exist. Cannot run v6 calculations.')



