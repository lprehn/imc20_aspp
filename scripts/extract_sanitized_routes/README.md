The scripts in this directory will allow you retrieve sanitized (prefix, path)
pairs---henceforth called routes---from your input data. 

This directory contains the following main scripts:

./get_sane_routes_v4.sh <date>
> This script will read all IPv4 related information from various gzip files
> in the same input dir. Those gzip files include informationen formatted in 
> BGPScanner's output format. The input dir is hardcoded and the expected 
> <date> parameter is in the form YYYYMMDD, e.g.,20200801. Data will be read
> from <inputdir>/<date>---this helped us to easily parallelize the job over 
> the days. By default, it reads from the directory that includes the sample
> data and outputs to a meta_data/ subdirectory. Apply changes as needed. 

./get_sane_routes_v4.sh <date> 
> This script does the same as the previous script but for v6 routes. 

python3 apply_base_sanitation.py 
> A script that applies all but one of our 
> sanitation steps. The only step not covered involves route collector 
> visibility---follow up scripts will calculate visibility and filter based
> on it on their own. This script is designed to read from a pipe.

python3 untangle_prefixes.py
> A script that reads routes and 'untangles' their prefix information
> i.e., when there are multiple prefixes in one route (e.g., when reading
> update messages with BGPScanner) it will generate multiple lines for the
> route such that there is one of the prefixes as well as the path in each 
> line. 
 
Running './get_sane_routes_v4.sh 20200801' for the sample data took on our mashine:

~~~
real    7m57.897s
user    11m16.768s
sys     0m25.171s
~~~

Similarly, running './get_sane_routes_v6.sh 20200801' took:

~~~
real    2m30.203s
user    4m22.471s
sys     0m13.706s
~~~
