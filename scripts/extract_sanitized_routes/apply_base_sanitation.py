import fileinput
from pytricia import PyTricia

def get_non_bogon_pytricia():
    # returns a PyTricia Trie with valid v6 address regions
    bogons = ["2000::/3","fc00::/7","ff00::/8"]
    pyt = PyTricia(128)
    for bogon in bogons:
        pyt[bogon] = 'a'
    return pyt

def get_bogon_pytricia():
    # returns a PyTricia Trie with invalid v4 address regions
    bogons = ["0.0.0.0/8","10.0.0.0/8","100.64.0.0/10","127.0.0.0/8","169.254.0.0/16","172.16.0.0/12","192.0.0.0/24","192.0.2.0/24","192.168.0.0/16","198.18.0.0/15","198.51.100.0/24","203.0.113.0/24","224.0.0.0/3"]
    pyt = PyTricia(128)
    for bogon in bogons:
        pyt[bogon] = 'a'
    return pyt

def is_bad_asn(asn):
    # checks if an ASN is invalid
    asn = int(asn)
    if asn == 0:
        return True
    if asn > 64495 and asn < 131072:
        return True
    if asn > 141625 and asn < 196608:
        return True
    if asn > 213403 and asn < 262144:
        return True
    if asn > 270748 and asn < 327680:
        return True
    if asn > 329727 and asn < 393216:
        return True
    if asn > 399260:
        return True
    return False


v6_valids = get_non_bogon_pytricia()
v4_invalids = get_bogon_pytricia()

for line in fileinput.input():

        try:
            pref, path = line.split('|',1)
        except:
            # status information, e.g., peer reset message
            continue

        # no path
        if len(path) == 0:
            continue

        # some status messages have a '|' in them ...
        if not '/' in pref:
            continue

        if ':' in pref:
            if pref not in v6_valids:
                # v6 bogon prefix
                continue
        else:
            if pref in v4_invalids:
                # v4 bogon prefix
                continue

        # default route?
        _, cidr = pref.split('/')
        if int(cidr) < 8:
            continue

        path = [x.rstrip() for x in path.split()]

        # line can't contain loop
        if len(path) < 3:
           print(line.rstrip())
           continue

        has_reserved_asn = False
        seen = set()
        loop = False
        for i in range(len(path)):
            try:
                if not '{' in path[i] and is_bad_asn(path[i]):
                    has_reserved_asn = True
                    break
                if path[i] in seen and not path[i] == path[i-1]:
                    loop = True
                    break
                seen.add(path[i])
            except:
                has_reserved_asn = True
                break
        if not loop and not has_reserved_asn:
            print(line.rstrip())
