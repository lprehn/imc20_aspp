#!/bin/bash

# stop on first error
set -e

DAY=${1}
INDIR=../../data_samples/bgp/ribs/
OUTFILE=../../meta_data/sane_routes/v6/sane_routes_v6_${DAY}.gz
TMPDIR=../../tmp/

# lets not overwrite existing results
if test -f "$FILE"; then
    echo "$FILE already exists";
else
    # the parallel part makes way more sense when slightly adjusted and used on our internal BGP download & rename pipeline.
    # depending on how unified your naming scheme / data is teh following might become hande: find . -name '*.gz' -o -name '*.bz2'
    # the uniq in the middle is helpful for updates which which only the communities changed
    # untangle_prefixes.py makes sure that there is only one (prefix,path)-pair per line (unlike BGPScanner's output for updates)
    parallel -a <( find ${INDIR} -name '*.gz') zcat | awk -F'|' '{if ($1 != "-"){print $2"|"$3}}' |  uniq |  python3 untangle_prefixes.py | grep ':' | sort --parallel 10 -T ${TMPDIR} | uniq | python3 apply_base_sanitation.py | gzip > ${OUTFILE}_tmp
    mv ${OUTFILE}_tmp ${OUTFILE}
    echo "Finished ${OUTFILE}.";
fi
