import fileinput

for line in fileinput.input():
    if not '|' in line or line.startswith('#'):
        continue
    prefs, path = line.split('|',2)
    if not ' ' in prefs:
        print(line.strip())
    else:
        prefixes = prefs.split(' ')
        for pref in prefixes:
            print('|'.join([pref, path.strip()]))
