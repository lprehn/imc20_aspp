# ASPP Usage
The scripts in this directory retrieve statistics about ASPP usage; 
they can further be used to produce the data displayed in Figure 
2 and 3 in the paper. 

# Scripts
python3 apply_monitor_limit.py \<date\> \<NumMonitors\> \<optional:'v6'\>
> this script reads all routes on <date> seen by more than <NumMonitors>
> monitors. By adding the argument 'v6' at the end, processed data will
> only be based on v6---it is only based on v4 otherwise. 
> The date is \<YYYYMMDD\>, NumMonitors is an Integer.  

python3 read_only_sane_prepended.py <date> <NumMonitors> <optional:'v6'>
> This script does the same as 'apply_monitor_limit.py', except that it only prints routes
> for which the path contains prepending. 

python3 read_only_sane_prepended_origin.py <date> <NumMonitors> <optional:'v6'>
> This script does the same as 'apply_monitor_limit.py', except that it only prints routes
> for which the path is prepended by the origin. 

python3 read_only_sane_prepended_intermediate.py <date> <NumMonitors> <optional:'v6'>
> This script does the same as 'apply_monitor_limit.py', except that it only prints routes
> for which the path is intermidiately prepended. 

# Required time
./get_usage_statistics.sh 20200801 30 

~~~
real    11m0.609s
user    11m52.870s
sys     0m37.940s
~~~

./get_usage_statistics.sh 20200801 30 v6

~~~
real    1m21.911s
user    1m27.837s
sys     0m3.516s
~~~

# Output
Both scripts produce output in the form:

~~~
20200801|797857|646924|156625|612251|2839572992|2385190144|741508352|2125477888|68442|17686|16995|3055
~~~

with the columns representing (in order):

1) The date
2) Number of unique prefixes
3) Number of unique prepended prefixes
4) Number of unique origin-prepended prefixes
5) Number of unique intermediately-prepended prefixes
6) Number of unique IPs
7) Number of unique prepended IPs
8) Number of unique origin-prepended IPs
9) Number of unique intermediately-prepended IPs
10) Number of unique ASes
11) Number of unique prepending ASes
12) Number of unique origin-prepending ASes
13) Number of unique intermediately-prepending ASes
