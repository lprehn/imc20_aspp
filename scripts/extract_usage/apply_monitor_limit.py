import sys, subprocess

def load_fully_vis_prefs(date):
    if len(sys.argv) == 4 and sys.argv[3] == 'v6':
        FILE = '../../meta_data/prepending_policies/origin_based/v6/prefix_origin/prefix_origin_polices_v6_'+date+'.gz'
    else:
        FILE = '../../meta_data/prepending_policies/origin_based/v4/prefix_origin/prefix_origin_polices_v4_'+date+'.gz'

    LIM=int(sys.argv[2])
    prefs = set()

    for line in zcat(FILE):
        if line.startswith('#'):
            continue
        pref, _, nummon, _, _ = line.split('|')
        if int(nummon) < LIM:
            continue
        prefs.add(pref)
    return prefs

def zcat(FILE):
    # reads entire file into memory using zcat
    process = subprocess.Popen(['zcat', FILE], stdout=subprocess.PIPE)
    for line in process.stdout.readlines():
        yield line.decode('utf-8').rstrip()


date = sys.argv[1]
full_view_prefs = load_fully_vis_prefs(date)

if len(sys.argv) == 4 and sys.argv[3] == 'v6':
    FILE = '../../meta_data/sane_routes/v6/sane_routes_v6_'+date+'.gz'
else:
    FILE = '../../meta_data/sane_routes/v4/sane_routes_v4_'+date+'.gz'

for line in zcat(FILE):
    try:
        pref, path_raw = line.split('|',1)
    except:
        continue # potential emptylines
    if pref.strip() not in full_view_prefs:
        continue
    print(line.rstrip())
