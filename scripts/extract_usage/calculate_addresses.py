import fileinput
from pytricia import PyTricia

def calc_address_space(pyt, v6):
    addresses = 0
    for pref in list(pyt):
        if pyt.parent(pref) is None:
            if not v6:
                addresses += 2**(32-int(pref.split('/')[1]))
            else:
                addresses += 2**(128-int(pref.split('/')[1]))
    return addresses

pyt = PyTricia(128)
v6 = False
for line in fileinput.input():
    if not v6:
        if ':' in line:
            v6 = True
    pyt[line.rstrip()] = 'a'

print(calc_address_space(pyt,v6))
