#!/bin/bash
#@1: <date>, as YYYYMMDD
#@2: <MonCount> as Integer
#@3: <v6> as word

# v6?
if [ "$#" -lt 3 ]; then
   v6=""
else
   v6=${3} 
fi

# get numbers for prefixes
PREFS_ALL=$(python3 apply_monitor_limit.py $1 $2 $v6 | cut -d '|' -f1 | sort | uniq | wc -l)
PREFS_PREP=$(python3 read_only_sane_prepended.py $1 $2 $v6 | cut -d '|' -f1 | sort | uniq | wc -l)
PREFS_ORIG=$(python3 read_only_sane_prepended_origin.py $1 $2 $v6 | cut -d '|' -f1 | sort | uniq | wc -l)
PREFS_INTER=$(python3 read_only_sane_prepended_intermediate.py $1 $2 $v6 | cut -d '|' -f1 | sort | uniq | wc -l)

# get all different values for ASes (the script itself calculates all at once )
ASES_VALS=$(python3 calculate_prepend_ases.py 20200801 30 $v6 )

# get numbers for prefixes
IPS_ALL=$(python3 apply_monitor_limit.py $1 $2 $v6 | cut -d '|' -f1 | sort | uniq | python3 calculate_addresses.py)
IPS_PREP=$(python3 read_only_sane_prepended.py $1 $2 $v6 | cut -d '|' -f1 | sort | uniq | python3 calculate_addresses.py)
IPS_ORIG=$(python3 read_only_sane_prepended_origin.py $1 $2 $v6 | cut -d '|' -f1 | sort | uniq | python3 calculate_addresses.py)
IPS_INTER=$(python3 read_only_sane_prepended_intermediate.py $1 $2 $v6 | cut -d '|' -f1 | sort | uniq | python3 calculate_addresses.py)

echo "${1}|${PREFS_ALL}|${PREFS_PREP}|${PREFS_ORIG}|${PREFS_INTER}|${IPS_ALL}|${IPS_PREP}|${IPS_ORIG}|${IPS_INTER}|${ASES_VALS}"
