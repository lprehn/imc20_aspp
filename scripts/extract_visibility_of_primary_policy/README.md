# Visibility of primary policy

The scripts in this directory allow you to calculate the visibility of each prefix-origin
combination as well as it's primary prepending policy. It can be used to reproduce the 
results needed for Figure 4 in the paper. 

# Scripts

utils.py
> this file provides methods to read the initial ribs and stream the "in order of
> appearence"- _ sorted_ updates. It is not supposed to be executed. 

python3 analyze_prepending_consistency.py
> This script does the main work. It relies strictly on utils.py for accessing all the
> data. Based on the initial ribs and the update stream it generates a set of 'events'
> (i.e., a combination of a timestamp, and the current prepending policy---which is -1 when
> currently not visible at any monitors) for each prefix-origin pair. In addtion to events,
> the code also tracks whether a monitor limit was hit for a given pair, 
> i.e., whether the prefix was visible by x (default = 200) monitors at any point in time. 
> The monitor number is hardcoded, to change it adjust the following fuction:
>
>> def seen_by_200_mons(pair):
>
> Finally, the scripts simply prints its multi-separator formated output (similar to a .csv)
> Please note: Since we sort all the updates, even running the code for the sample data takes
> 45 minutes (see below). At some pleaces in the code we decided to extensively use RAM to
> gain a performance speed-up. Still, when you run this code with all collector data of an
> entire month (i.e., you redo the measurement from our paper) the code easily runs 3-4 days
> and peeks at above 200GB RAM.  

python3 calc_availability_and_consistency.py <output_of_above_script.csv>
> This script reads the events from the above script's output and calculates for how long
> each prefix-origin pair was visible, what is its primary policy, for how long it stayed
> in its primary policy, and whether it reached the 'fully visible' monitor limit. 


# Required time
python3 analyze_prepending_consistency.py > ../../meta_data/visibility/visibility.csv

~~~
real    43m53.965s
user    42m28.388s
sys     2m6.816s
~~~

python3 calc_availability_and_consistency.py ../../meta_data/visibility/visibility.csv

~~~
real    0m7.012s
user    0m6.878s
sys     0m0.127s
~~~

# Output

~~~
# pref, origin, time_visible, primary_policy, policy_visibility, hit_mon_limit
174.4.48.0/22,6327,1.0,0,1.0,0
79.180.143.0/24,8551,1.0,2,1.0,0
170.239.2.0/23,52878,1.00,2,0.55,0
81.12.224.0/21,12302,1.0,0,1.0,0
~~~

pref - prefix <\br>
origin - the AS that originated the prefix <\br>
time_visible - the fraction of time that this prefix-origi combination was
visible by at least one monitor <\br>
primary_policy - the policy that was used for the most amount of time <\br>
policy_visibility - fraction of $time_visible for which the entry was in its 
primary policy <\br>
hit_mon_limit - 1 if entry was seen by at least X (default=200) monitors, 0
otherwise
