from utils import MRTDayStream
from collections import defaultdict
import gc


'''
in the end we want to get for every prefix,origin pair a list of tuples:
(policy, timeunits), where policy is :
    -1:    if the pair for that timespan was not announced
     0:    if the pair was not seeing any prepending
     1:    if the pair sees uniform prepending
     2:    if the pair has binary prepending
     3:    if the pair sees diverse prepending
'''

def get_num_origin_prepends(path):
    # simply calculates the number of origin prepends in a path
    prepend = 0
    for i in  list(range(len(path)))[:-1][::-1]:
        if path[i] == path[-1]:
            prepend += 1
        else:
            return prepend
    return prepend

def build_initial_policy_table(rib):
    ''' 
    builds a policy table for every prefix+origin pair
    to more easily keep track of the policy changes we track policies in the following data structure:
    policytable[(pref, origin)] => dict[policy] => list(feeders that currently see this policy)
    '''
    policy_table = defaultdict(lambda: defaultdict(set))
    for fd in rib:
        for pref in rib[fd]:
            policy_table[(pref, rib[fd][pref][-1])][ min(3,get_num_origin_prepends(rib[fd][pref]))].add(fd)
    return policy_table

def build_initial_state(MDS, v4_only = False):
    '''
    reads the initial state from the rib files
    and generates with the following structure:
    global_rib[(FASN, FIP)] => dict[prefs] => set(paths)
    where FASN and FIP are the feeder asn and ip, and 
    prefs are prefixes.
    '''

    global_rib = defaultdict(lambda: dict()) 
    for line in MDS.initial_state():
        elems = line.split('|')
        prefix = elems[1]

        # check if this run is only for v4 or v6
        if v4_only:
            if ':' in prefix:
                continue

        # ignore paths with sets
        path = elems[2]
        if '{' in path:
            continue
        else:
            path = [int(x) for x in path.split()]

        feeder = elems[8]
        fds = feeder.split(' ')
        if len(fds) > 2:    # ignore add-path session (isolario has also normal sessions for all their add-path feeders)
            continue 

        global_rib[tuple(fds)][prefix] = path

    return global_rib, int(elems[9].split('.')[0])

def policy_type(policies):
    '''
    calculates the current prepending policy that is
    visible from all feeds.
    '''

    # check for each policy if it is currently seen by any feeds
    vals = []
    for policy in policies:
        if len(policies[policy]) > 0:
            vals.append(policy)
    vals = sorted(vals)

    # calculate policy.
    if len(vals) == 1:
        if vals[0] == 0:
            return 0
        else:
            return 1
    if len(vals) == 2:
        return 2
    else:
        return 3

def tuples_to_string(tuplelist):
    # produces a string representation for a set of tuples
    strlist = []
    for elem in tuplelist:
        strlist.append(str(elem[0])+','+str(elem[1]))
    return '|'.join(strlist)

def negative_return_int_dict():
    # generates a directory with negative default value
    x = defaultdict(int)
    x.default_factory = -1
    return x

def seen_by_200_mons(pair):
    #checks if a prefix-origin pair is seen by 200 or more monitors
    total = 0
    for pol in pair.keys():
        total += len(pair[pol])
    return total > 200    # !!! adjust this line to change the monitor limit !!!


MDS = MRTDayStream("20200801", "20200802")

# read the initial rib state and calculate initial policies
global_rib, init_ts = build_initial_state(MDS) 
policies = build_initial_policy_table(global_rib)

# get the set of currently fully visible prefixes
fully_visible_pairs = set()
for pairid in policies.keys():
    if seen_by_200_mons(policies[pairid]):
        fully_visible_pairs.add(pairid)

# since we needed the global rib only for calculating the initial
# policies etc, let's make sure its memory is freed quickly.
gc.collect()

events = defaultdict(list)    # list of all events
policy_types = defaultdict(int)
policy_types.default_factory = -1

# those two data structures provide additional reprepsentations of the
# already generated data structures. Rather than iterating a single data
# structure's keys, they help to quickly maneuver between data points
# we traded significant amounts of RAM against runtime benefits...
feeder_tab = defaultdict(lambda: defaultdict(set)) # Holds all the prefix-origin pairs currently seen by each feeder
pairid_by_pref = defaultdict(set) # holds all prefix-origin pairs aggregated by prefix

# fill the data RAM abusive structures
for pairid in policies:
    pairid_by_pref[pairid[0]].add(pairid)
    policy_types[pairid] = policy_type(policies.get(pairid))
    for polid in policies[pairid]:
        for feeder in policies[pairid][polid]:
            feeder_tab[feeder][pairid] = polid


# updates, here we come. 
for line in MDS.update_stream():

    # extract data
    elems = line.split('|')
    type = elems[0]
    if type == '#':
        continue
    prefix = elems[1]
    path = elems[2]
    ts = int(elems[-2].split('.')[0])
    if '{' in path:
            continue
    path = [int(x) for x in path.split()]
    feeder = elems[-3]
    if feeder.count(' ') >1: # again, no add-path sessions
        continue 
    fd = tuple(feeder.split())

    # withdrawl
    if type == '-':
        for pairid in pairid_by_pref.get(prefix, []):
            pair = policies[pairid]
            last_seen_pol = feeder_tab[fd].get(pairid,-1)
            if last_seen_pol == -1: # this fd never saw the pair
                continue
            else:
                prev_pair_pol = policy_types.get(pairid)
                pair[last_seen_pol].remove(fd) 
                feeder_tab[fd][pairid] = -1
                new_pair_pol = policy_type(pair)
                if prev_pair_pol != new_pair_pol:
                    events[pairid].append( (prev_pair_pol, ts) )
                    policy_types[pairid] = new_pair_pol 
                else:
                    policy_types[pairid] = new_pair_pol 
                    continue
    else:
        try:
            # skips, e.g., peer-state comments
            num_prep = min(3,get_num_origin_prepends(path))
            pairid = (prefix, path[-1])
        except:
             continue
        pair = policies.get((prefix, path[-1]))
        if pair is None:    # the pair was not seen previously!
            events[(prefix, path[-1])].append( (-1, ts) )
            policies[(prefix, path[-1])][num_prep].add(fd)
            feeder_tab[fd][(prefix, path[-1])] = num_prep
            pairid_by_pref[prefix].add((prefix, path[-1]))

        else:
            if num_prep in pair:    # another monitor observed a currently already existing policy.
                last_seen_pol = feeder_tab[fd].get(pairid,-1)
                if last_seen_pol == -1: # this fd never saw the pair
                    feeder_tab[fd][pairid] = num_prep
                    pair[num_prep].add(fd)
                    if seen_by_200_mons(pair):
                        fully_visible_pairs.add(pairid)
                else: # update that changes the policy of an fd. 
                    prev_pair_pol = policy_types.get((prefix, path[-1]))
                    pair[last_seen_pol].remove(fd)
                    feeder_tab[fd][pairid] = num_prep 
                    pair[num_prep].add(fd)
                    new_pair_pol = policy_type(pair) 
                    if prev_pair_pol != new_pair_pol: # a change in the entire policy! 
                        events[(prefix, path[-1])].append( (prev_pair_pol, ts) )
                        policy_types[(prefix, path[-1])] = new_pair_pol

            else: # a more diverse policy was found!
                prev_pair_pol = policy_types.get((prefix, path[-1]))
                pair[num_prep].add(fd)
                feeder_tab[fd][pairid] = num_prep 
                new_pair_pol = policy_type(pair) 
                if seen_by_200_mons(pair):
                    fully_visible_pairs.add(pairid)
                events[(prefix, path[-1])].append( (prev_pair_pol, ts) )
                policy_types[(prefix, path[-1])] = new_pair_pol

# we are done with all the fancy update parsing... 
# now lets add the last seen policy.
for pairid in policies:
    pol = policy_types.get(pairid)
    events[pairid].append( (pol, ts) )

# finally, let's save the data.
for pairid in events:
    if pairid in fully_visible_pairs:
        fulvis = '1'
    else:
        fulvis = '0'
    linestring = str(pairid[0])+','+str(pairid[1])+'#'
    linestring += tuples_to_string(events.get(pairid))
    print(linestring+'|'+fulvis)

