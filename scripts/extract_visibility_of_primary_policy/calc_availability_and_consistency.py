import sys
from collections import defaultdict

def get_pol(raw):
    try: 
        return int(raw)
    except:
        # might be none
        return -1

def get_primary_vals(start, fulltime, events):
    # start is the start time
    # fulltime is the entire time
    # events is the list of events (e.g. policy changes)
    ts = []
    pols = []
    time_in_pols = defaultdict(int)
    for event in events:
        pol_raw, ts_raw = event.split(',')
        pols.append(get_pol(pol_raw))
        ts.append(int(ts_raw))

    # add the start and then all the other vals
    time_in_pols[0] += ts[0] - start
    for i in range(1, len(pols)):
        time_in_pols[pols[i]] += ts[i]-ts[i-1]

    time_not_vis = time_in_pols.get(-1,0)
    vis_time = fulltime-time_not_vis
    pref_vis = float(vis_time)/float(fulltime)

    max_time_in_pol = 0
    for pol in time_in_pols:
        if pol == -1:
            continue
        if max_time_in_pol < time_in_pols[pol]:
            prim_pol = pol
            max_time_in_pol = time_in_pols[pol]
    pol_vis = float(max_time_in_pol)/float(vis_time)
    if pol_vis < 0.000001:
        print(float(max_time_in_pol), float(vis_time))
    return pref_vis, prim_pol, pol_vis

def handle_line(line, start, fulltime):
    key, data = line.split('#')
    events = data.split('|')[:-1]
    if len(events) == 1 :
        try:
            pol = int(events[0].split(',')[0])
        except:
            pol = -1
        print(key+',1.0,'+str(pol)+','+'1.0'+','+data.split('|')[-1].strip())
    else:
        pref_vis, prim_pol, pol_vis = get_primary_vals(start, fulltime, events)
        print(key+','+"{:.2f}".format(pref_vis)+','+str(prim_pol)+','+"{:.2f}".format(pol_vis)+','+data.split('|')[-1].strip())

stop = -1
start = 1000000000000 
#start = 1596240000
#fulltime = 1596412800 - start

with open(sys.argv[1], 'r') as INPUT:
    for line in INPUT:
        if line.startswith('#'):
            continue
        events = line.split('#')[1].split('|')[:-1]
        for event in events:
            _, ts = event.split(',')
            ts = int(ts)
            start = min(ts, start)
            stop = max(ts, stop)
fulltime = stop-start

print('# pref, origin, time_visible, primary_policy, policy_visibility')
with open(sys.argv[1], 'r') as INPUT:
    for line in INPUT:
        if line.startswith('#'):
            continue
        else:
            handle_line(line, start, fulltime)
