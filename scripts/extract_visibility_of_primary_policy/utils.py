from __future__ import print_function
from subprocess import Popen, PIPE
import sys, os, datetime, gzip, io, subprocess


def zcat(FILE):
    # reads entire file into memory using zcat
    process = subprocess.Popen(['zcat', FILE], stdout=subprocess.PIPE)
    for line in process.stdout.readlines():
        yield line.decode('utf-8').rstrip()

def untangle_prefs(line):
    ''' 
    untangles prefixes, i.e., one route per line
    for bgpscanner output that may have multiple
    prefixes per update
    '''
    type, prefs, remainder = line.split('|',2)
    if not ' ' in prefs:
        yield line
    else:
        prefixes = prefs.split(' ')
        for pref in prefixes:
            yield '|'.join([type, pref, remainder])

class MRTDayStream(object):
    def __init__(self, startdate, enddate, data_root = '../../data_samples/bgp/'):

        start = datetime.datetime.strptime(startdate, "%Y%m%d")
        end = datetime.datetime.strptime(enddate, "%Y%m%d")
        date_generator = [start + datetime.timedelta(days=x) for x in range(0, (end-start).days+1)]

        # set the necessary vars
        self.dates = sorted([ date.strftime("%Y%m%d") for date in date_generator])
        self.root = data_root

    def initial_state(self):
        yield from read_ribs(self.root+'/ribs/', self.dates[0])

    def update_stream(self):

        for date in self.dates:
            for hour in ["%02d" % x for x in list(range(24))]:
                updates = []
                for collector in os.listdir(self.root+'/updates/'+date+'/'):
                    file = self.root+'/updates/'+date+'/'+collector+'/updates.'+date+'.'+hour+'00.gz'
                    updates.extend(list(zcat(file)))
                for line in sorted(updates, key = lambda x: int(x.rsplit('|',2)[1])):
                    yield from untangle_prefs(line)

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def read_ribs(dir, day):
     for entry in os.listdir(dir+'/'+day+'/'):
         if '.rib.gz' in entry and day in entry:
             yield from zcat(os.path.join(dir+'/'+day+'/', entry))


