# Peering Schedules

We used the scripts in this directory to set up our peering experiements.
We used a dedicated VM that ran the peering testbed client software:

https://github.com/PEERINGTestbed/client

and also generated the probing traffic while capturing the responses. 
While you can execute those scripts as they are, they will need various
adjustments to work as intended given your actual setup (e.g., assigned PEERING 
resources).

# Scripts (and their minimum necessary adjustments)

python3 get_exp_crontab_and_timings.py
> this script automatically generates the crontab necessary to run the
> PoP vs PoP experiments, as well as the one vs many experiments (in both
> directions) for both schedule types. The functions that generate 
> PRE-schedule experiments contain the word 'reverse' while the 
> POST-schedule functions do not. When executing the script, it will
> print the crontab to stdout while writing a metadata file called 
> 'timings.csv' will be generated in the same directory. We used the timings file to identify
> which type of experiment, which PoP combination, which prepending size, or 
> which schedule was executed at a certain time using which of the prefixes. 
> The script needs _at least_ the follwing adjustments to be useful (with the
> exemplary code snippets to adjust):
> 1) change the start time for your experiments:
>> current_time = datetime.datetime(2020, 8, 31, 16, 0) 
> 2) change the prefixes made available to your experiments by the Peering Staff:
>> prefixes = ['184.164.240.0/24', '184.164.241.0/24', '184.164.242.0/24']
> 3) Adjust all of the links for the executables:
>> action_a = "sudo /root/client_untouched/client/peering prefix announce -m "+mux_prep+ses_prep_str+" "+prefix <br>
>> action_b = "sudo /root/client_untouched/client/peering prefix announce -m "+mux_norm+ses_norm_str+" "+prefix <br>
>> action_c = "sudo python3 /root/ASPP_filtering_exp/configure_routing_for_experiment.py" <br> 
> 4) Update the list of muxes and upstreams you want to use
>> MUXES=['grnet01', 'gatech01', 'clemson01', 'amsterdam01', 'seattle01', 'ufmg01', 'neu01', 'utah01', 'uw01', 'wisc01'] <br>
>> sids =['178',     '15',        '190',      '60',          '101',       '16',     '182',   '424',    '181',  '184']
> 5) Set an appropriate 'Mailto' address:
>> print('MAILTO=xxx@yyy.de')

python3 get_hijack_exp_crontab.py
> this one is similar to the one above, it produces the crontabs for the 
> hijacking results. All of the 5 changes from the last script also need 
> to be performed for this script. Executing the script will write the 
> crontab to stdout (with some comments on what is happening when)

python3 configure_routing_for_experiment.py <-d>
> this script generates routing rules that ensure that all pings are routed via 
> a tunnel to the peering testbed. When adding '-d' at the end ir removes such rules.
> this files reads a list of target IPs and aggregates them to a list of /16 prefixes
> based on which it then generates the routing rules. It currently expects a csv file
> which contains a list of IP targets on the fourth column of each row (we had the
> targets of within a given AS in one line, thus it reads all lines.) The file that
> the script reads is hardcoded. replace the "put_your_target_set_here" string with a 
> path to your target file.

python3 run_pings.py
> requires: Nping version 0.7.40 ( https://nmap.org/nping ) to be installed
> this script runs ping probes using IMCP, UDP, and TCP to each target, once per minute.
> It reading of the targets is the same as for the last script. Please replace 
> "put_your_target_set_here" with a reference to your targets file.

python3 start_capture.py
> requires: tcpdump version 4.9.2
> --------- libpcap version 1.8.1
> starts a bunch of tcpdump captures (one per MUX) and terminates them after 40 min.
> change the generic filenames to generate in the line "p = subprocess.Popen(..." to
> match your preferences for storing the pcaps.  


# Required time

The first three scripts should take a few seconds towards a minute 
(depending on your number of targets/iteration).

the run_pings.py script schedules a bunch of nping calls and lives for about 30 minutes. 

the start_capture.py script takes a little more than 40 minutes, for which it seelps most
of the time. 
