import subprocess, sys

def do_config(prefix):
    if len(sys.argv) > 1 and sys.argv[1] == '-d':
        subprocess.run(["ip","route","del",prefix,"via","100.70.128.1","dev","tap6"])
    else:
        subprocess.run(["ip","route","add",prefix,"via","100.70.128.1","dev","tap6"])

slash_16s = set()
with open('put_your_target_set_here', 'r') as INPUT:
    for line in INPUT.readlines():
        if line.startswith('#') or len(line.strip())< 1:
            continue
        _, _, _, ips = line.split('|')
        if len(ips.strip()) < 1:
            continue
        for ip in ips.split(','):
            if ':' in ip:
                continue
            slash_16s.add('.'.join(ip.split('.')[:2])+'.0.0/16')

for prefix in slash_16s:
    do_config(prefix)

