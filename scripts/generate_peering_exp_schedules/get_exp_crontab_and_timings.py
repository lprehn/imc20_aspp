import datetime

# starttime of the experiments
current_time = datetime.datetime(2020, 8, 31, 16, 0)

# all usuable prefixes
prefixes = ['184.164.240.0/24', '184.164.241.0/24', '184.164.242.0/24']

# generates the session string for the cli
def get_session_str(sessions):
    if sessions is None:
        return ""
    if isinstance(sessions, str):
        return  " -c 47065,"+sessions
    thestr = ""
    for session in sessions:
        thestr += " -c 47065,"+session
    return thestr

# resets the time to before the last experiment (useful for scheduling multiple in parallel)
def reset_current_time():
    global current_time
    current_time -= datetime.timedelta(minutes = 90)


def generate_experiment_string(prefix, mux_prep, mux_norm, sessions_prep = None, sessions_norm = None, P = 0, PRNT = True, capture = True):
    global current_time
    ses_prep_str =  get_session_str(sessions_prep)
    ses_norm_str =  get_session_str(sessions_norm)

    # Minute 00: announce prefix via all upstream unprepended
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 15)

    action_a = "sudo /root/client_untouched/client/peering prefix announce -m "+mux_prep+ses_prep_str+" "+prefix
    action_b = "sudo /root/client_untouched/client/peering prefix announce -m "+mux_norm+ses_norm_str+" "+prefix
    action_c = "sudo python3 /root/ASPP_filtering_exp/configure_routing_for_experiment.py"
    if PRNT:
        print(timestr+action_a)
        print(timestr+action_b)
    if PRNT and capture:
        print(timestr+action_c)

    # Minute 15: announce prefix prepended via other upstream
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 10)
    if P > 0:
        action = "sudo /root/client_untouched/client/peering prefix announce -m "+mux_prep+ses_prep_str+" -P "+str(P)+" "+prefix
        if PRNT:
            print(timestr+action)

    # Minute 25: start tcpdump
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 5)
    action = "sudo python3 /root/ASPP_filtering_exp/start_capture.py"
    if PRNT and capture:
        print(timestr+action)

    # Minute 30: start pings
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 30)
    action = "sudo python3 /root/ASPP_filtering_exp/run_pings.py"
    if PRNT and capture:
        print(timestr+action)

    # Minute 60: send withdraw.
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 30)
    action = "sudo /root/client_untouched/client/peering prefix withdraw "+prefix
    if PRNT:
        print(timestr+action+'\n')

def generate_reverse_experiment_string(prefix, mux_prep, mux_norm, sessions_prep = None, sessions_norm = None, P = 0, PRNT = True, capture = True):
    global current_time
    ses_prep_str =  get_session_str(sessions_prep)
    ses_norm_str =  get_session_str(sessions_norm)

    # Minute 00: announce prefix via all upstream unprepended
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 15)

    if P > 0:
        action_a = "sudo /root/client_untouched/client/peering prefix announce -m "+mux_prep+ses_prep_str+" -P "+str(P)+" "+prefix
    else:
        action_a = "sudo /root/client_untouched/client/peering prefix announce -m "+mux_prep+ses_prep_str+" "+prefix

    #action_b = "sudo /root/client_untouched/client/peering prefix announce -m "+mux_norm+ses_norm_str+" "+prefix
    action_b = "sudo python3 /root/ASPP_filtering_exp/configure_routing_for_experiment.py"
    if PRNT:
        print(timestr+action_a)
        if capture:
            print(timestr+action_b)
        #print(timestr+action_c)

    # Minute 15: announce prefix prepended via other upstream
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 10)
    action = "sudo /root/client_untouched/client/peering prefix announce -m "+mux_norm+ses_norm_str+" "+prefix
    if PRNT:
        print(timestr+action)

    # Minute 25: start tcpdump
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 5)
    action = "sudo python3 /root/ASPP_filtering_exp/start_capture.py"
    if PRNT and capture:
        print(timestr+action)

    # Minute 30: start pings
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 30)
    action = "sudo python3 /root/ASPP_filtering_exp/run_pings.py"
    if PRNT and capture:
        print(timestr+action)

    # Minute 60: send withdraw.
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 30)
    action = "sudo /root/client_untouched/client/peering prefix withdraw "+prefix
    if PRNT:
        print(timestr+action+'\n')


def generate_many_vs_one_experiment_string(prefix, muxes, sessions, idx, P = 0, capture = True):
    global current_time

    # Minute 00: announce prefix via all upstream unprepended
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 15)

    for i in range(len(muxes)):
        ses_prep_str =  get_session_str(sessions[i])
        action = "sudo /root/client_untouched/client/peering prefix announce -m "+muxes[i]+ses_prep_str+" "+prefix
        print(timestr+action)
    action_c = "sudo python3 /root/ASPP_filtering_exp/configure_routing_for_experiment.py"
    if capture:
        print(timestr+action_c)

    # Minute 15: announce prefix prepended via other upstream
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 10)
    if P > 0:
        ses_prep_str =  get_session_str(sessions[idx])
        action = "sudo /root/client_untouched/client/peering prefix announce -m "+muxes[idx]+ses_prep_str+" -P "+str(P)+" "+prefix
        print(timestr+action)

    # Minute 25: start tcpdump
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 5)
    action = "sudo python3 /root/ASPP_filtering_exp/start_capture.py"
    if capture:
        print(timestr+action)

    # Minute 30: start pings
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 30)
    action = "sudo python3 /root/ASPP_filtering_exp/run_pings.py"
    if capture:
        print(timestr+action)

    # Minute 60: send withdraw.
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 30)
    action = "sudo /root/client_untouched/client/peering prefix withdraw "+prefix
    print(timestr+action+'\n')


def generate_reverse_many_vs_one_experiment_string(prefix, muxes, sessions, idx, P = 0, capture= True):
    global current_time

    # Minute 00: announce prefix via prepended upstream
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 15)

    ses_prep_str =  get_session_str(sessions[idx])
    if P > 0:
        action = "sudo /root/client_untouched/client/peering prefix announce -m "+muxes[idx]+ses_prep_str+" -P "+str(P)+" "+prefix
    else:
       action = "sudo /root/client_untouched/client/peering prefix announce -m "+muxes[idx]+ses_prep_str+" "+prefix
    print(timestr+action)


    # Minute 15: announce prefix prepended via other upstream
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 10)

    for i in range(len(muxes)):
        if i != idx:
            ses_prep_str = get_session_str(sessions[i])
            action = "sudo /root/client_untouched/client/peering prefix announce -m "+muxes[i]+ses_prep_str+" "+prefix
            print(timestr+action)
    action_c = "sudo python3 /root/ASPP_filtering_exp/configure_routing_for_experiment.py"
    if capture:
        print(timestr+action_c)


    # Minute 25: start tcpdump
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 5)
    action = "sudo python3 /root/ASPP_filtering_exp/start_capture.py"
    if capture:
        print(timestr+action)

    # Minute 30: start pings
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 30)
    action = "sudo python3 /root/ASPP_filtering_exp/run_pings.py"
    if capture:
        print(timestr+action)

    # Minute 60: send withdraw.
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 30)
    action = "sudo /root/client_untouched/client/peering prefix withdraw "+prefix
    print(timestr+action+'\n')


def generate_one_vs_many_experiment_string(prefix, muxes, sessions, idx, P = 0, capture = True):
    global current_time

    # Minute 00: announce prefix via all upstream unprepended
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 15)

    for i in range(len(muxes)):
        ses_prep_str =  get_session_str(sessions[i])
        action = "sudo /root/client_untouched/client/peering prefix announce -m "+muxes[i]+ses_prep_str+" "+prefix
        print(timestr+action)
    action_c = "sudo python3 /root/ASPP_filtering_exp/configure_routing_for_experiment.py"
    if capture:
        print(timestr+action_c)

    # Minute 15: announce prefix prepended via other upstream
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 10)
    if P > 0:
        for i in range(len(muxes)):
            if i == idx:
                continue
            ses_prep_str =  get_session_str(sessions[i])
            action = "sudo /root/client_untouched/client/peering prefix announce -m "+muxes[i]+ses_prep_str+" -P "+str(P)+" "+prefix
            print(timestr+action)

    # Minute 25: start tcpdump
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 5)
    action = "sudo python3 /root/ASPP_filtering_exp/start_capture.py"
    if capture:
        print(timestr+action)

    # Minute 30: start pings
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 30)
    action = "sudo python3 /root/ASPP_filtering_exp/run_pings.py"
    if capture:
        print(timestr+action)

    # Minute 60: send withdraw.
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 30)
    action = "sudo /root/client_untouched/client/peering prefix withdraw "+prefix
    print(timestr+action+'\n')




def generate_reverse_one_vs_many_experiment_string(prefix, muxes, sessions, idx, P = 0, capture = True):
    global current_time

    # Minute 00: announce prefix via all upstream unprepended
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 15)

    for i in range(len(muxes)):
        if i == idx:
            continue
        ses_prep_str =  get_session_str(sessions[i])
        if P > 0:
            action = "sudo /root/client_untouched/client/peering prefix announce -m "+muxes[i]+ses_prep_str+" -P "+str(P)+" "+prefix
        else:
            action = "sudo /root/client_untouched/client/peering prefix announce -m "+muxes[i]+ses_prep_str+" "+prefix
        print(timestr+action)

    # Minute 15: announce prefix prepended via other upstream
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 10)

    ses_prep_str =  get_session_str(sessions[idx])
    action = "sudo /root/client_untouched/client/peering prefix announce -m "+muxes[idx]+ses_prep_str+" "+prefix
    print(timestr+action)
    action_c = "sudo python3 /root/ASPP_filtering_exp/configure_routing_for_experiment.py"
    if capture:
        print(timestr+action_c)

    # Minute 25: start tcpdump
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 5)
    action = "sudo python3 /root/ASPP_filtering_exp/start_capture.py"
    if capture:
        print(timestr+action)

    # Minute 30: start pings
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 30)
    action = "sudo python3 /root/ASPP_filtering_exp/run_pings.py"
    if capture:
        print(timestr+action)

    # Minute 60: send withdraw.
    timestr = current_time.strftime("%M %H %d %m ")+'* '
    current_time += datetime.timedelta(minutes = 30)
    action = "sudo /root/client_untouched/client/peering prefix withdraw "+prefix
    print(timestr+action+'\n')

'''
MUX          | ASN   | Node Degree  | Session ID | Transit?
-------------|-------|--------------|------------|----------
GRnet01      | 5408  | ~40          | 178        | Yes
Gatech       | 2637  | ~6           | 15         | Yes
Clemson      | 12148 | ~4           | 190        | Yes
Amsterdam01  | 12859 | ~1k          | 60         | Yes
Amsterdam01  | 8283  | ~800         | 94         | Yes
Seattle01    | 3130  | ~200         | 101        | Yes
Ufmg01       | 1916  | ~1.9k        | 16         | Yes
Neu01        | 156   | ~5           | 182        | Yes 
Utah01       | 210   | ~20          | 424        | Yes
uw01         | 101   | ~60          | 181        | Yes
wisc01       | 3128  | ~15          | 184        | Yes
isi01        | 226   | ~50          | 14         | Yes
'''

MUXES=['grnet01', 'gatech01', 'clemson01', 'amsterdam01', 'seattle01', 'ufmg01', 'neu01', 'utah01', 'uw01', 'wisc01']
sids =['178',     '15',        '190',      '60',          '101',       '16',     '182',   '424',    '181',  '184']

timings = []
print('MAILTO=xxx@yyy.de')
print('############################')
prefix_idx = 0
N = len(prefixes)
for i in range(len(sids)):
    for j in range(len(sids)):
        if i != j and MUXES[i]!=MUXES[j]:
            for P in [0,1,2,3]:
                start_time = current_time.strftime("%Y-%m-%d-%H-%M")
                if prefix_idx != N-1:
                    generate_experiment_string(prefixes[prefix_idx], MUXES[i],MUXES[j], sids[i], sids[j], P = P, capture = False)
                    end_time = current_time.strftime("%Y-%m-%d-%H-%M")
                    reset_current_time()
                else:
                    generate_experiment_string(prefixes[prefix_idx], MUXES[i],MUXES[j], sids[i], sids[j], P = P, capture = True)
                    end_time = current_time.strftime("%Y-%m-%d-%H-%M")
                timings.append('|'.join(["0","0", start_time, end_time, prefixes[prefix_idx], MUXES[i], MUXES[j], str(P)]))
                prefix_idx = (prefix_idx +1) % N

for i in range(len(sids)):
    for j in range(len(sids)):
        if i != j and MUXES[i]!=MUXES[j]:
            for P in [0,1,2,3]:
                start_time = current_time.strftime("%Y-%m-%d-%H-%M")
                if prefix_idx != N-1:
                    generate_reverse_experiment_string(prefixes[prefix_idx], MUXES[i],MUXES[j], sids[i], sids[j], P = P, capture = False)
                    end_time = current_time.strftime("%Y-%m-%d-%H-%M")
                    reset_current_time()
                else:
                    generate_reverse_experiment_string(prefixes[prefix_idx], MUXES[i],MUXES[j], sids[i], sids[j], P = P, capture = True)
                    end_time = current_time.strftime("%Y-%m-%d-%H-%M")
                timings.append('|'.join(["0", "1", start_time, end_time, prefixes[prefix_idx], MUXES[i], MUXES[j], str(P)]))
                prefix_idx = (prefix_idx +1) % N

for i in range(len(MUXES)):
    for P in [0,1,2,3]:
        start_time = current_time.strftime("%Y-%m-%d-%H-%M")
        if prefix_idx != N-1:
            generate_many_vs_one_experiment_string(prefixes[prefix_idx], MUXES, sids, i, P, capture = False) 
            end_time = current_time.strftime("%Y-%m-%d-%H-%M")
            reset_current_time()
        else:
            generate_many_vs_one_experiment_string(prefixes[prefix_idx], MUXES, sids, i, P, capture = True)
            end_time = current_time.strftime("%Y-%m-%d-%H-%M")
        timings.append('|'.join(["1", "0", start_time, end_time, prefixes[prefix_idx], 'ALL', MUXES[i], str(P)]))
        prefix_idx = (prefix_idx +1) % N


for i in range(len(MUXES)):
    for P in [0,1,2,3]:
        start_time = current_time.strftime("%Y-%m-%d-%H-%M")
        if prefix_idx != N-1:
            generate_reverse_many_vs_one_experiment_string(prefixes[prefix_idx], MUXES, sids, i, P, capture = False)
            end_time = current_time.strftime("%Y-%m-%d-%H-%M")
            reset_current_time()
        else:
            generate_reverse_many_vs_one_experiment_string(prefixes[prefix_idx], MUXES, sids, i, P, capture = True)
            end_time = current_time.strftime("%Y-%m-%d-%H-%M")
        timings.append('|'.join(["1", "1", start_time, end_time, prefixes[prefix_idx], 'ALL', MUXES[i], str(P)]))
        prefix_idx = (prefix_idx +1) % N


for i in range(len(MUXES)):
    for P in [0,1,2,3]:
        start_time = current_time.strftime("%Y-%m-%d-%H-%M")
        if prefix_idx != N-1:
            generate_one_vs_many_experiment_string(prefixes[prefix_idx], MUXES, sids, i, P, capture = False)
            end_time = current_time.strftime("%Y-%m-%d-%H-%M")
            reset_current_time()
        else:
            generate_one_vs_many_experiment_string(prefixes[prefix_idx], MUXES, sids, i, P, capture = True)
            end_time = current_time.strftime("%Y-%m-%d-%H-%M")
        timings.append('|'.join(["2","0", start_time, end_time, prefixes[prefix_idx], MUXES[i], 'ALL', str(P)]))
        prefix_idx = (prefix_idx +1) % N


for i in range(len(MUXES)):
    for P in [0,1,2,3]:
        start_time = current_time.strftime("%Y-%m-%d-%H-%M")
        if prefix_idx == N-1 or (i == len(MUXES)-1 and P == 3):
            generate_reverse_one_vs_many_experiment_string(prefixes[prefix_idx], MUXES, sids, i, P, capture = True)
            end_time = current_time.strftime("%Y-%m-%d-%H-%M")
        else:
            generate_reverse_one_vs_many_experiment_string(prefixes[prefix_idx], MUXES, sids, i, P, capture = False)
            end_time = current_time.strftime("%Y-%m-%d-%H-%M")
            reset_current_time()
        timings.append('|'.join(["2","1", start_time, end_time, prefixes[prefix_idx], MUXES[i], 'ALL', str(P)]))

        prefix_idx = (prefix_idx +1) % N

with open('./timings.csv', 'w+') as out:
    for timing in timings:
        out.write(timing+'\n')
