from datetime import datetime, timedelta

# muxes
muxes = ['amsterdam01', 'seattle01', 'clemson01', 'gatech01', 'grnet01']

# get a list of all combinations
mux_combs = list()
for i in range(len(muxes)):
    for j in range(i, len(muxes)):
        if i != j:
            mux_combs.append((muxes[i], muxes[j]))
            mux_combs.append((muxes[j], muxes[i]))

# set the current date
current = datetime(2020, 1, 13, 16)

# simply increases time in one hour steps
def next_timestamp():
    global current
    rtstring = "00 "+str("{:02d}".format(current.hour)) + ' ' +str("{:02d}".format(current.day)) + ' ' +str("{:02d}".format(current.month)) +  ' *'
    current += timedelta(hours=1)
    return rtstring

print('MAILTO=xxx@yyy.de')
# make sure to change those only to ASes you are allowed to use
origin_orig, origin_leak = "61574", "61575"

for (original, leak) in mux_combs:
    print('####################### Original: '+str(original)+', leak: '+str(leak)+' '+"#######################")
    leakstr = "sudo ~/client_untouched/client/peering prefix announce -m "+leak+" -o "+origin_leak+"  184.164.242.0/24"
    cleanstr = "sudo ~/client_untouched/client/peering prefix withdraw 184.164.242.0/24"
    for i in range(4):
        timestr = next_timestamp()
        if i == 0:
            actionstr = "sudo ~/client_untouched/client/peering prefix announce -m "+original+" -o "+origin_orig+" 184.164.242.0/24"
        else:
            actionstr = "sudo ~/client_untouched/client/peering prefix announce -m "+original+" -o "+origin_orig+" -P "+str(i+1)+" 184.164.242.0/24"
        print('# '+['1st', '2nd', '3rd', '4th'][i]+' round')
        print(timestr+' '+actionstr)
        print('1'+timestr[1:]+' '+leakstr)
        print('3'+timestr[1:]+' '+cleanstr+'\n')
    print('\n')


