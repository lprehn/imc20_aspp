import subprocess
import sched, time

Targets = set()
def to_chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    new = []
    for i in range(0, len(lst), n):
         new.append(lst[i:i + n])
    return new

def send_pings(targets):
    global Targets
    packets = str(len(Targets))
    # 184.164.240.0/24
    args = ["nping","-c","1","--icmp","--rate", packets, "-data-string", "stop pings via email to lprehn<at>mpi-inf.mpg.de", "--source-ip", "184.164.240.10","--quiet","--no-capture"]
    args.extend(targets)
    subprocess.run(args)

    args = ["nping","-c","1","--tcp","-p","80","--rate", packets, "-data-string", "stop pings via email to lprehn<at>mpi-inf.mpg.de", "--source-ip", "184.164.240.10","--quiet","--no-capture"]
    args.extend(targets)
    subprocess.run(args)

    args = ["nping","-c","1","--udp","--rate", packets, "-data-string", "stop pings via email to lprehn<at>mpi-inf.mpg.de", "--source-ip", "184.164.240.10","--quiet","--no-capture"]
    args.extend(targets)
    subprocess.run(args)

    # 184.164.241.0/24
    args = ["nping","-c","1","--icmp","--rate", packets, "-data-string", "stop pings via email to lprehn<at>mpi-inf.mpg.de", "--source-ip", "184.164.241.10","--quiet","--no-capture"]
    args.extend(targets)
    subprocess.run(args)

    args = ["nping","-c","1","--tcp","-p","80","--rate", packets, "-data-string", "stop pings via email to lprehn<at>mpi-inf.mpg.de", "--source-ip", "184.164.241.10","--quiet","--no-capture"]
    args.extend(targets)
    subprocess.run(args)

    args = ["nping","-c","1","--udp","--rate", packets, "-data-string", "stop pings via email to lprehn<at>mpi-inf.mpg.de", "--source-ip", "184.164.241.10","--quiet","--no-capture"]
    args.extend(targets)
    subprocess.run(args)

    # 184.164.242.0/24
    args = ["nping","-c","1","--icmp","--rate", packets, "-data-string", "stop pings via email to lprehn<at>mpi-inf.mpg.de", "--source-ip", "184.164.242.10","--quiet","--no-capture"]
    args.extend(targets)
    subprocess.run(args)

    args = ["nping","-c","1","--tcp","-p","80","--rate", packets, "-data-string", "stop pings via email to lprehn<at>mpi-inf.mpg.de", "--source-ip", "184.164.242.10","--quiet","--no-capture"]
    args.extend(targets)
    subprocess.run(args)

    args = ["nping","-c","1","--udp","--rate", packets, "-data-string", "stop pings via email to lprehn<at>mpi-inf.mpg.de", "--source-ip", "184.164.242.10","--quiet","--no-capture"]
    args.extend(targets)
    subprocess.run(args)

with open('put_your_target_set_here', 'r') as INPUT:
    for line in INPUT.readlines():
        if line.startswith('#'):
            continue
        _, _, _, ips = line.split('|')
        for ip in ips.split(','):
            if ':' in ip or not '.' in ip:
                continue
            Targets.add(ip.rstrip())

#send_pings("8.8.8.8")

Targets = list(Targets)
# Initialize the scheduler

target_batches = to_chunks(Targets, 200)
N = len(target_batches)

s = sched.scheduler(time.time, time.sleep)
Minutes = 30

offset = float(60.0/(float(N)))
for min in range(Minutes):
    for i in range(N):
        print(str(min*N+i)+'/'+str((min*N+i)*offset)+'/'+str(N))
        s.enter((min*N+i)*offset, 1, send_pings, argument=(target_batches[i],))
s.run()

