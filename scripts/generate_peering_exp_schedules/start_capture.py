import time, subprocess

def start_dumps():
    taps = ['tap1','tap10','tap11','tap14','tap16','tap17','tap18','tap2','tap4','tap5','tap6','tap7','tap9']
    pids = []
    for tap in taps:
        p = subprocess.Popen(['tcpdump','-i',tap,'-G','3600','-w','/root/ASPP_filtering_exp/traffic_dumps/'+tap+'/'+tap+'_dump_%Y-%m-%d-%H-%M.pcap','-n', 'dst', '184.164.240.10','or', '184.164.241.10','or', '184.164.242.10' ], stdout=subprocess.PIPE)
        pids.append(p)
    return pids

def terminate(pids):
    for p in pids:
        p.terminate()

pids = start_dumps()
# capture for 40 mins...
time.sleep(40*60)
terminate(pids)
